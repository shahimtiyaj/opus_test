package shahimtiyaj.opus_test.database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import shahimtiyaj.opus_test.app.AppController;
import shahimtiyaj.opus_test.model.GetItem;

import static shahimtiyaj.opus_test.database.DBHelper.TABLE_GETITEMLIST;

public class DAO {
    private static final String TAG = DAO.class.getSimpleName();

    // Database fields
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public DAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        if (db != null && db.isOpen())
            db.close();
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Cursor getRecordsCursor(String sql, String[] param) {
        Cursor curs = null;
        curs = db.rawQuery(sql, param);
        return curs;
    }


    public void execSQL(String sql, String[] param) throws SQLException {
        db.execSQL(sql, param);
    }

    public static void executeSQL(String sql, String[] param) {
        DAO da = new DAO(AppController.getInstance());
        da.open();
        try {
            da.execSQL(sql, param);
        } catch (Exception e) {
            throw e;
        } finally {
            da.close();
        }
    }



    public ArrayList<GetItem> GetItemName() {
        ArrayList<GetItem> messagesArrayList = new ArrayList<GetItem>();
        GetItem message = null;
        Cursor curs = null;

        try {

            curs = db.query(TABLE_GETITEMLIST , new String[]{"[ID]", "[ItemCatID]",
                    "[ItemCode]", "[CodeWithName]", "[ItemName]", "[ItemSpac]", "[ItemDescription]", "[ReOrderLevel]", "[UnitName]", "[SubCatID]",
                    "[SubCatName]", "[MCatID]", "[MainCatName]", "[ItemCatName]"}, null, null, null, null, null);


            if (curs.moveToFirst()) {
                do {
                    message = new GetItem(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getString(4), curs.getString(5),
                            curs.getString(6), curs.getString(7), curs.getString(8),
                            curs.getString(9), curs.getString(10), curs.getString(11),
                            curs.getString(12), curs.getString(13));
                    messagesArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messagesArrayList;
    }

    public ArrayList<GetItem> ShowValue(String itemName) {
        ArrayList<GetItem> messagesArrayList = new ArrayList<GetItem>();
        GetItem message = null;
        Cursor curs = null;

        try {

            curs = db.query(TABLE_GETITEMLIST + " WHERE ItemName = '"+ itemName+"'", new String[]{"[ID]", "[ItemCatID]",
                    "[ItemCode]", "[CodeWithName]", "[ItemName]", "[ItemSpac]", "[ItemDescription]", "[ReOrderLevel]", "[UnitName]", "[SubCatID]",
                    "[SubCatName]", "[MCatID]", "[MainCatName]", "[ItemCatName]"}, null, null, null, null, null);


            if (curs.moveToFirst()) {
                do {
                    message = new GetItem(curs.getString(0), curs.getString(1), curs.getString(2),
                            curs.getString(3), curs.getString(4), curs.getString(5),
                            curs.getString(6), curs.getString(7), curs.getString(8),
                            curs.getString(9), curs.getString(10), curs.getString(11),
                            curs.getString(12), curs.getString(13));
                    messagesArrayList.add(message);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return messagesArrayList;
    }


}
