package shahimtiyaj.opus_test.model;

public class GetItem {

    private String ID;
    private String ItemCatID;
    private String ItemCode;
    private String CodeWithName;
    private String ItemName;
    private String ItemSpac;
    private String ItemDescription;
    private String ReOrderLevel;
    private String UnitName;
    private String SubCatID;
    private String SubCatName;
    private String MCatID;
    private String MainCatName;
    private String ItemCatName;


    public GetItem() {

    }

    public GetItem(String ID, String ItemCatID, String ItemCode, String CodeWithName, String ItemName,
                   String ItemSpac, String ItemDescription, String ReOrderLevel, String UnitName,
                   String SubCatID, String SubCatName, String MCatID,String MainCatName,
                   String ItemCatName ) {
        this.ID = ID;
        this.ItemCatID = ItemCatID;
        this.ItemCode = ItemCode;
        this.CodeWithName=CodeWithName;
        this.ItemName=ItemName;
        this.ItemSpac = ItemSpac;
        this.ItemDescription = ItemDescription;
        this.ReOrderLevel = ReOrderLevel;
        this.UnitName = UnitName;
        this.SubCatID = SubCatID;
        this.SubCatName = SubCatName;
        this.MCatID=MCatID;
        this.MainCatName=MainCatName;
        this.ItemCatName = ItemCatName;
    }


    public String getID() {
        return ID;
    }

    public String setID(String ID) {
        this.ID = ID;
        return ID;
    }

    public String getItemCatID() {
        return ItemCatID;
    }

    public String setItemCatID(String itemCatID) {
        ItemCatID = itemCatID;
        return itemCatID;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public String setItemCode(String itemCode) {
        ItemCode = itemCode;
        return itemCode;
    }

    public String getCodeWithName() {
        return CodeWithName;
    }

    public String setCodeWithName(String codeWithName) {
        CodeWithName = codeWithName;
        return codeWithName;
    }

    public String getItemName() {
        return ItemName;
    }

    public String setItemName(String itemName) {
        ItemName = itemName;
        return itemName;
    }

    public String getItemSpac() {
        return ItemSpac;
    }

    public String setItemSpac(String itemSpac) {
        ItemSpac = itemSpac;
        return itemSpac;
    }

    public String getItemDescription() {
        return ItemDescription;
    }

    public String setItemDescription(String itemDescription) {
        ItemDescription = itemDescription;
        return itemDescription;
    }

    public String getReOrderLevel() {
        return ReOrderLevel;
    }

    public String setReOrderLevel(String reOrderLevel) {
        ReOrderLevel = reOrderLevel;
        return reOrderLevel;
    }

    public String getUnitName() {
        return UnitName;
    }

    public String setUnitName(String unitName) {
        UnitName = unitName;
        return unitName;
    }

    public String getSubCatID() {
        return SubCatID;
    }

    public String setSubCatID(String subCatID) {
        SubCatID = subCatID;
        return subCatID;
    }

    public String getSubCatName() {
        return SubCatName;
    }

    public String setSubCatName(String subCatName) {
        SubCatName = subCatName;
        return subCatName;
    }

    public String getMCatID() {
        return MCatID;
    }

    public String setMCatID(String MCatID) {
        this.MCatID = MCatID;
        return MCatID;
    }

    public String getMainCatName() {
        return MainCatName;
    }

    public String setMainCatName(String mainCatName) {
        MainCatName = mainCatName;
        return mainCatName;
    }

    public String getItemCatName() {
        return ItemCatName;
    }

    public String setItemCatName(String itemCatName) {
        ItemCatName = itemCatName;
        return itemCatName;
    }


}
