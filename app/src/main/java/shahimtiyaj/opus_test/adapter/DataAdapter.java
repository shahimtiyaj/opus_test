package shahimtiyaj.opus_test.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import shahimtiyaj.opus_test.R;
import shahimtiyaj.opus_test.app.AppController;
import shahimtiyaj.opus_test.database.DAO;
import shahimtiyaj.opus_test.model.GetItem;

public class DataAdapter  extends BaseAdapter {
    String[] result;
    private ArrayList<GetItem> categoryArrayList;
    Context context;
    private static LayoutInflater inflater = null;

    public DataAdapter(Context context, ArrayList<GetItem> categoryArrayList) {
        // TODO Auto-generated constructor stub
        this.categoryArrayList = categoryArrayList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return categoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView category_type, item_code;

    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();

        final GetItem category = (GetItem) categoryArrayList.get(position);
        final DAO dao = new DAO(AppController.getInstance());
        dao.open();
        final String categoryItemCatName= category.getItemCatName();
        final String categoryCode = category.getItemCode();

        View rowView;
        rowView = inflater.inflate(R.layout.activity_main, null);
        holder.category_type = (TextView) rowView.findViewById(R.id.item_category);
        holder.item_code = (TextView) rowView.findViewById(R.id.item_code);

        holder.category_type.setText(categoryArrayList.get(position).getItemCatName());
        holder.item_code.setText(categoryArrayList.get(position).getItemCode());


        return rowView;
    }

}
