package shahimtiyaj.opus_test.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import shahimtiyaj.opus_test.R;
import shahimtiyaj.opus_test.adapter.DataAdapter;
import shahimtiyaj.opus_test.app.AppController;
import shahimtiyaj.opus_test.database.DAO;
import shahimtiyaj.opus_test.model.GetItem;
import shahimtiyaj.opus_test.networkservice.CloudRequest;

import static com.android.volley.VolleyLog.v;
import static shahimtiyaj.opus_test.app.AppController.showDebugDBAddressLogToast;
import static shahimtiyaj.opus_test.database.DBHelper.TABLE_GETITEMLIST;

/**
 * Created by shahimtiyaj on 27/8/2018.
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    ArrayList<GetItem> categoryArrayList;
    ArrayList<String> itemCategoryArrayList;
    private Spinner selectionCategory;
    TextView item_category,item_code;
    private  String spinner_category;
    private String selectitem, item_id, pos;
    private DataAdapter adapter;
    ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initView();
    }

    public void initView() {
        categoryArrayList = new ArrayList<GetItem>();
        showDebugDBAddressLogToast(getApplicationContext());
        GetItemList();
        selectionCategory = (Spinner) findViewById(R.id.spinner_category);
        selectionCategory.setOnItemSelectedListener(this);
        setItemNameSpinnerData(categoryArrayList);
        item_category=(TextView)findViewById(R.id.item_category);
        item_code=(TextView)findViewById(R.id.item_code);;


    }

    private void setItemNameSpinnerData(ArrayList<GetItem> categoryArrayList) {
        DAO dao = new DAO(getApplicationContext());
        dao.open();
        ArrayList<String> itemCategoryName = new ArrayList<>();

        categoryArrayList = dao.GetItemName();

        for(int i =0 ; i<categoryArrayList.size();i++){
            itemCategoryName.add(categoryArrayList.get(i).getItemName());
        }

        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,itemCategoryName);
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectionCategory.setAdapter(locationAdapter);

        dao.close();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        DAO dao = new DAO(getApplicationContext());
        dao.open();
        ArrayList<GetItem> itemArrayList = new ArrayList<GetItem>();


        Spinner spinner = (Spinner)parent;
        if (spinner.getId()==R.id.spinner_category){
            spinner_category = selectionCategory.getSelectedItem().toString();

            pos = String.valueOf(position);

            item_id = parent.getItemAtPosition(position).toString();

            ShowItem(item_id);

            Toast.makeText(getApplicationContext(),item_id,Toast.LENGTH_LONG).show();

        }

    }

    public  void ShowItem(String item_id){

        DAO dao = new DAO(getApplicationContext());
        dao.open();
        dao.ShowValue(item_id);

        for(int i =0 ; i<categoryArrayList.size();i++){
            item_category.setText(categoryArrayList.get(i).getItemCatName());
            item_code.setText(categoryArrayList.get(i).getItemCode());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void GetItemList() {
        String url = AppController.getGetCategoryUrl();

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "GetItemList from server Response: " + response.toString());

                        try {
                            v("Response:%n %s", response.toString(4));
                            DAO dao = new DAO(getApplicationContext());
                            dao.open();

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject getItemList = (JSONObject) response
                                        .get(i);

                                GetItem item = new GetItem();

                                String id = item.setID(getItemList.getString("ID"));
                                String itemCatID = item.setItemCatID(getItemList.getString("ItemCatID"));
                                String itemCode = item.setItemCode(getItemList.getString("ItemCode"));
                                String codeWithName = item.setCodeWithName(getItemList.getString("CodeWithName"));
                                String itemName = item.setItemName(getItemList.getString("ItemName"));
                                String itemSpac = item.setItemSpac(getItemList.getString("ItemSpac"));
                                String itemDescription = item.setItemDescription(getItemList.getString("ItemDescription"));
                                String reOrderLevel = item.setReOrderLevel(getItemList.getString("ReOrderLevel"));
                                String unitName = item.setUnitName(getItemList.getString("UnitName"));
                                String subCatID = item.setSubCatID(getItemList.getString("SubCatID"));
                                String subCatName = item.setSubCatName(getItemList.getString("SubCatName"));
                                String mCatID = item.setMCatID(getItemList.getString("MCatID"));
                                String mainCatName = item.setMainCatName(getItemList.getString("MainCatName"));
                                String itemCatName = item.setItemCatName(getItemList.getString("ItemCatName"));

                                categoryArrayList.add(item);


                                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_GETITEMLIST +
                                                "(ID, ItemCatID, ItemCode, CodeWithName, ItemName, ItemSpac, ItemDescription, " +
                                                "ReOrderLevel, UnitName, SubCatID, SubCatName, MCatID, MainCatName, ItemCatName) " +
                                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                        new String[]{id, itemCatID, itemCode, codeWithName, itemName, itemSpac, itemDescription,
                                                reOrderLevel, unitName, subCatID, subCatName, mCatID, mainCatName, itemCatName});
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.i("Volley error:", volleyError.toString());
                Log.d(TAG, "Error: " + volleyError.getMessage());

                if (volleyError instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                } else if (volleyError instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                } else if (volleyError instanceof TimeoutError) {
                    Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                }
            }
        });

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

        CloudRequest.getInstance(this).addToRequestQueue(req);
    }



    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
