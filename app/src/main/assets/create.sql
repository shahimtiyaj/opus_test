CREATE TABLE [getitemlist] (
	[ID] Integer PRIMARY KEY,
	[ItemCatID] Integer NULL,
	[ItemCode] nvarchar(50) NULL,
	[CodeWithName] nvarchar(100) NULL,
	[ItemName] nvarchar(100) NULL,
	[ItemSpac] nvarchar(50) NULL,
	[ItemDescription] nvarchar(50) NULL,
	[ReOrderLevel] double NULL,
	[UnitName] nvarchar(50) NULL,
	[SubCatID] Integer NULL,
	[SubCatName] nvarchar(50) NULL,
	[MCatID] Integer NULL,
	[MainCatName] nvarchar(50) NULL,
	[ItemCatName] nvarchar(50) NULL
);

